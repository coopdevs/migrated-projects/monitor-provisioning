## Goal
Provision and maintain an intermediary Prometheus between hosts and Grafana Cloud's "hosted
metrics".

## Roles used
* `coopdevs.sys-admins-role` to manage the sys admin users
* `geerlingguy.security` to configure security
* `cloudalchemy.prometheus` to scrape configs and remote writing
* `coopdevs.monitoring_role` to expose metrics of itself
* `cloudalchemy.grafana` to visualize of the metrics
* `cloudalchemy.blackbox-exporter` to expose probing metrics of endpoints over HTTP, HTTPS, DNS, TCP and ICMP.

## Requirements
### Pyenv and virtualenv

We use Pyenv and Virtualenv to manage the Python version and isolate the project packages and dependencies:

1. Install Pyenv + Virtualenv
2. Create a virtualenv:
```bash
pyenv virtualenv 3.9.5 monitor-provisioning
```
3. Install Ansible and Galaxy dependencies:
```bash
pip install -r ansible-requirements.txt
ansible-galaxy install -r requirements.yml
```

## Usage
```bash
ansible-playbook playbooks/provision.yml -i inventory/hosts -l prod --ask-vault-pass
```

## Development
Install [devenv](https://github.com/coopdevs/devenv/) to run a local lxc
container.

```bash
# Create a "connected" lxc container with python installed.
devenv
# Provision the main playbook
ansible-playbook playbooks/provision.yml -i inventory/hosts -l dev
```
Bear in mind that the test prometheus is configured to only monitor itself.

Once provisioned you'll have a Grafana at http://monitor.local:3000 consuming
from Prometheus, whose web UI you can check at http://monitor.local:9090

## Configuration

### Prometheus Jobs
#### BlackBox Exporter

To use [BlackBox Exporter](https://github.com/prometheus/blackbox_exporter) define the [modules](https://github.com/prometheus/blackbox_exporter/blob/master/CONFIGURATION.md#module) to use into the probes. We use the module `http_2xx`, but we need redefine it because we use `ipv4` and the default protocol used by the module is the `ipv6`.

Then define the jobs in your `inventory/host_var/` configuration pointing to the local exporter server.
The BlackBox Exporter needs to be passed the target as a parameter, this can be done with relabelling: https://github.com/prometheus/blackbox_exporter#prometheus-configuration
Add the next relabel configuration to the scrape jobs:
```
    relabel_configs:
    - source_labels: [__address__]
      target_label: __param_target
    - source_labels: [__param_target]
      target_label: instance
    - target_label: __address__
      replacement: localhost:9115  # The blackbox exporter's real hostname:port.

```
### Postgresql Exporter
To acquire metrics of Postgresql Exporter from a new target you have to define a new item in `psql_scrape_list`. There, `name` is the internal identifier of the scrape job. `metrics_path` is the endpoint after the host where the job gets the data from. `user` and `password` are the Basic Auth credentials to access the data. Finaly `url` is the domain of the target. Here is an example:
```yaml
psql_scrape_list:
  - name: "test_pexporter"
    metrics_path: "/postgres"
    user: "username" # Usually in a secret
    password: "password123" # Usually in a secret
    url: "http://test.example.org" # Usually in a secret
```

#### Prometheus Alert Rules

Define rules to raise alerts when these rules are not fulfilled.

Use the variable `prometheus_alert_rules` to define the rules:

```yml
prometheus_alert_rules:
  - alert: Watchdog
    expr: vector(1)
    for: 10m
    labels:
      severity: warning
    annotations:
      description: "This is an alert meant to ensure that the entire alerting pipeline is functional.\nThis alert is always firing, therefore it should always be firing in Alertmanager\nand always fire against a receiver. There are integrations with various notification\nmechanisms that send a notification when this alert is not firing. For example the\n\"DeadMansSnitch\" integration in PagerDuty."
      summary: 'Ensure entire alerting pipeline is functional'
```

#### Use AlertManager to send notifications

Using the variable `prometheus_alertmanager_config` we can define the URL and the port of our AlertManager service:

```yml
prometheus_alertmanager_config:
  - scheme: http
    static_configs:
      - targets:
          # Alertmanager's default port is 9093
          - localhost:9093
```

### AlertManager

We want to use [AlertManager](https://www.prometheus.io/docs/alerting/latest/alertmanager) as notification sender.

In AlertManager you need to define a receiver and a route to send the alert to the receiver, who notificates the team.

Define the route using the variable `alertmanager_route`:

```yml
alertmanager_route:
    group_wait: 5s
    group_interval: 5s
    repeat_interval: 5s
    group_by: ['alertname', 'cluster', 'service']
    receiver: localhost
```

This route points to a `localhost` receiver, we need to define it using the variable `alertmanager_receivers`:

```yml
alertmanager_receivers:
    - name: localhost
      webhook_configs:
          - url: 'http://localhost:9999'
```

The receiver `localhost` is just an example, you can define the receiver and use the hook that you need. Find all the hooks in [AlertManager Receivers doc](https://www.prometheus.io/docs/alerting/latest/configuration/#receiver)

#### Integration with Zulip

We create a bot in Zulip to send the notification to concrete stream following the next instructions:
https://zulip.com/integrations/doc/alertmanager

In order to use the same bot in more than one channel you need to construct the URL as in the documentation by editing the stream name to send the notification to different channels.

URL example:

https://coopdevs.zulipchat.com/api/v1/external/alertmanager?api_key=****&stream=ChannelName
